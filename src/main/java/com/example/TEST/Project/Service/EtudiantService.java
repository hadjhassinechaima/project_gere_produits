package com.example.TEST.Project.Service;

import com.example.TEST.Project.Repository.EtudiantRepository;
import com.example.TEST.Project.model.Etudiant;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class EtudiantService {
    private final EtudiantRepository repo;

    public EtudiantService(EtudiantRepository repo) {
        this.repo = repo;
    }
    public Etudiant addEtudiant(Etudiant e) {
        return repo.save(e);
    }

}
