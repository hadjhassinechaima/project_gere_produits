package com.example.TEST.Project.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Evaluer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long IdEva;
    private  String note;

    public Long getIdEva() {
        return IdEva;
    }

    public void setIdEva(Long idEva) {
        IdEva = idEva;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    @Override
    public String toString() {
        return "Evaluer{" +
                "IdEva=" + IdEva +
                ", note='" + note + '\'' +
                '}';
    }
}
