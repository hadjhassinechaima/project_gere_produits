package com.example.TEST.Project.model;

import org.springframework.data.jpa.repository.JpaRepository;

import javax.persistence.*;

@Entity
public class Matiere {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long CodeMat;
    private String Coefficient;

    public Long getCodeMat() {
        return CodeMat;
    }

    public void setCodeMat(Long codeMat) {
        CodeMat = codeMat;
    }

    public String getCoefficient() {
        return Coefficient;
    }

    public void setCoefficient(String coefficient) {
        Coefficient = coefficient;
    }
   public Matiere(){

   }
    @Override
    public String toString() {
        return "Matiere{" +
                "CodeMat=" + CodeMat +
                ", Coefficient='" + Coefficient + '\'' +
                '}';
    }
    @ManyToOne(cascade = {CascadeType.MERGE})
    @JoinColumn(name = "CodeEnseignant")
    private  Enseignant enseignant;
    @ManyToOne(cascade = {CascadeType.MERGE})
    @JoinColumn(name = "Codeetudiant")
    private  Enseignant enseignant;


}

