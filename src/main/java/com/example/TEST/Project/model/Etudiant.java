package com.example.TEST.Project.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Set;

@Entity
public class Etudiant {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private  Long CodeEtu;
    private String Nom;
    private String Prenom;

    public  Etudiant(){

    }

    public Long getCodeEtu() {
        return CodeEtu;
    }

    public void setCodeEtu(Long codeEtu) {
        CodeEtu = codeEtu;
    }

    public String getNom() {
        return Nom;
    }

    public void setNom(String nom) {
        Nom = nom;
    }

    public String getPrenom() {
        return Prenom;
    }

    public void setPrenom(String prenom) {
        Prenom = prenom;
    }

    @Override
    public String toString() {
        return "Etudiant{" +
                "CodeEtu=" + CodeEtu +
                ", Nom='" + Nom + '\'' +
                ", Prenom='" + Prenom + '\'' +
                '}';
    }
    @JsonIgnore
    @OneToMany(mappedBy = "etudiant",cascade = {CascadeType.MERGE})
    private Set<Etudiant> E;

}
