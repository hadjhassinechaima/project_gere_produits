package com.example.TEST.Project.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Entity
public class Enseignant{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private  Long CodEns;
    private  String email;

    public Long getCodEns() {
        return CodEns;
    }

    public void setCodEns(Long codEns) {
        CodEns = codEns;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    public  Enseignant(){

    }
    @Override
    public String toString() {
        return "Enseignant{" +
                "CodEns=" + CodEns +
                ", email='" + email + '\'' +
                '}';
    }
    @JsonIgnore
    @OneToMany(mappedBy = "enseignant",cascade = {CascadeType.MERGE})
    private Set<Matiere> m;


}
