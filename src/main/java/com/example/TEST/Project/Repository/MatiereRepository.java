package com.example.TEST.Project.Repository;

import com.example.TEST.Project.model.Matiere;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MatiereRepository extends JpaRepository<Matiere,Long> {
    Matiere findMatiereByCodeMat(Long CodeMat);
}
