package com.example.TEST.Project.Repository;

import com.example.TEST.Project.model.Etudiant;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EtudiantRepository extends JpaRepository<Etudiant, Long> {
    Etudiant findEtudiantByCodeEtu (Long CodeEtu);
}
