package com.example.TEST.Project.Repository;

import com.example.TEST.Project.model.Enseignant;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EnseignantRepository extends JpaRepository<Enseignant ,Long> {
    Enseignant findEnseignantByCodEns(Long  CodEns);
}
